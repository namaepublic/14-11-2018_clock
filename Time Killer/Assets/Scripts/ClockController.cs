﻿using UnityEngine;

public class ClockController : MonoBehaviour
{
    [SerializeField] private Transform _hoursNeedle, _minutesNeedle;
    [SerializeField] private float _hoursSpeed, _minutesSpeed;

    private int _speedCoef;

    private void FixedUpdate()
    {
        _hoursNeedle.Rotate(Vector3.forward, _hoursSpeed * Time.deltaTime * _speedCoef);
        _minutesNeedle.Rotate(Vector3.forward, _minutesSpeed * Time.deltaTime * _speedCoef);
    }

    public void ReverseTime(bool reverse)
    {
        _speedCoef = reverse ? 1 : -1;
    }
}