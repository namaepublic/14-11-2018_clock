﻿using UnityEngine;
using UnityEngine.UI;

public class TimeSliderController : MonoBehaviour
{
    [SerializeField] private Image _negativeFill, _positiveFill;
    [SerializeField] private ClockController _clockController;
    [SerializeField] private Slider _timeSlider;
    
    private float _timeValue;

    public float TimeValue
    {
        get { return _timeValue; }
        set
        {
            _timeValue = value;
            _timeValue = Mathf.Clamp(_timeValue, -1, 1);
            
            Time.timeScale = Mathf.Abs(_timeValue);
            Time.fixedDeltaTime = 0.02F * Time.timeScale;
            
            if (_timeValue < 0)
            {
                _clockController.ReverseTime(true);
                _negativeFill.fillAmount = -_timeValue;
                _positiveFill.fillAmount = 0;
            }
            else
            {
                _clockController.ReverseTime(false);
                _positiveFill.fillAmount = _timeValue;
                _negativeFill.fillAmount = 0;
            }
        }
    }

    private void OnValidate()
    {
        _clockController = FindObjectOfType<ClockController>();
    }

    public void Init()
    {
        _timeSlider.interactable = true;
    }

    public void Stop()
    {
        _timeSlider.interactable = false;
    }

    public void ResetSlider()
    {
        _timeSlider.value = 0;
        _timeSlider.interactable = true;
    }
}
