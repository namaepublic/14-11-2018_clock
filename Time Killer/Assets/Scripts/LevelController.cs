﻿using UnityEngine;

public class LevelController : MonoBehaviour
{
	[SerializeField] private int _numberOfTargets;
	[SerializeField] private LevelsController _levelsController;

	private int _hitCount;

	private void OnValidate()
	{
		_levelsController = FindObjectOfType<LevelsController>();
	}

	public bool HitTarget()
	{
		_hitCount++;
		if (_hitCount < _numberOfTargets) return false;
		
		_levelsController.NextLevel();
		return true;

	}
}
