﻿using System.Collections;
using TMPro;
using UnityEngine;

public class BulletController : MonoBehaviour
{
    [SerializeField] private LevelsController _levelsController;
    [SerializeField] private Rigidbody2D _rigidbody2D;
    [SerializeField] private Vector2 _startPosition;
    [SerializeField] private float _speed;
    [SerializeField] private int _startTimer;
    [SerializeField] private TextMeshProUGUI _timerText;
    [SerializeField] private TimeSliderController _timeSliderController;

    [SerializeField] [HideInInspector] private int _deadZoneLayer, _targetLayer;
    private bool _init;

    private void OnValidate()
    {
        _levelsController = FindObjectOfType<LevelsController>();
        _timeSliderController = FindObjectOfType<TimeSliderController>();
        _rigidbody2D = GetComponent<Rigidbody2D>();
        _deadZoneLayer = LayerMask.NameToLayer("Obstacle");
        _targetLayer = LayerMask.NameToLayer("Target");
        transform.position = _startPosition;
        if (_timerText != null)
            _timerText.text = _startTimer.ToString();
    }

    public IEnumerator Init()
    {
        _timerText.gameObject.SetActive(true);
        for (var i = 0; i < _startTimer; i++)
        {
            _timerText.text = (_startTimer - i).ToString();
            yield return new WaitForSecondsRealtime(1);
        }

        _timerText.gameObject.SetActive(false);
        _init = true;
        _timeSliderController.Init();
    }

    public void Stop()
    {
        _timerText.gameObject.SetActive(false);
        _init = false;
        _rigidbody2D.position = _startPosition;
        _timeSliderController.Stop();
    }

    private void FixedUpdate()
    {
        if (!_init) return;
        if (_timeSliderController.TimeValue > 0)
            _rigidbody2D.position += Vector2.right * _speed * Time.deltaTime;
        else
            _rigidbody2D.position += Vector2.left * _speed * Time.deltaTime;

        if (_rigidbody2D.position.x < _startPosition.x)
            _rigidbody2D.position = _startPosition;
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (!_init) return;
        if (other.gameObject.layer == _deadZoneLayer)
            _levelsController.RestartLevel();

        if (other.gameObject.layer == _targetLayer && !_levelsController.CurrentLevel.HitTarget())
            other.gameObject.SetActive(false);
    }
}