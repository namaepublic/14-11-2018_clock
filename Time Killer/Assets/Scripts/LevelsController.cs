﻿using UnityEngine;

public class LevelsController : MonoBehaviour
{
    [SerializeField] private LevelController[] _levels;
    [SerializeField] private GameController _gameController;
    [SerializeField] private BulletController _bulletController;

    private int _level;

    public LevelController CurrentLevel
    {
        get { return _levels[_level - 1]; }
    }

    private void OnValidate()
    {
        _levels = GetComponentsInChildren<LevelController>(true);
        _gameController = FindObjectOfType<GameController>();
        _bulletController = FindObjectOfType<BulletController>();
    }

    public void ResetLevels()
    {
        _level = 0;
        Init();
    }

    private void Init()
    {
        _bulletController.Stop();

        foreach (var levelController in _levels)
            levelController.gameObject.SetActive(false);
    }
    
    public void NextLevel()
    {
        if (_level >= _levels.Length)
        {
            _gameController.GameOver();
            return;
        }
        Init();
        
        _levels[_level].gameObject.SetActive(true);
        _level++;
        StartCoroutine(_bulletController.Init());
    }

    public void RestartLevel()
    {
        _level--;
        NextLevel();
    }
}