﻿using UnityEditor;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameController : MonoBehaviour
{
    [SerializeField] private LevelsController _levelsController;
    [SerializeField] private TimeSliderController _timeSliderController;
    [SerializeField] private GameObject _menuPanel;

    private bool _started;

    private void OnValidate()
    {
        _levelsController = FindObjectOfType<LevelsController>();
        _timeSliderController = FindObjectOfType<TimeSliderController>();
    }

    private void Start()
    {
        Init();
    }

    private void Update()
    {
        if (_started) return;

        if (_timeSliderController.TimeValue >= 1)
            StartGame();
        if (_timeSliderController.TimeValue <= -1)
            Quit();
    }

    public void Init()
    {
        _levelsController.ResetLevels();
        _timeSliderController.ResetSlider();
        _menuPanel.SetActive(true);
        _started = false;
    }

    public void StartGame()
    {
        _started = true;
        _menuPanel.SetActive(false);
        _levelsController.NextLevel();
    }

    public void GameOver()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    public void Quit()
    {
#if UNITY_EDITOR
        EditorApplication.isPlaying = false;
#else
        Application.Quit();
#endif
    }
}